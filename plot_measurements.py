import numpy as np
import pandas as pd
from mpl_toolkits import mplot3d
from matplotlib import cm
import matplotlib.pyplot as plt

data = pd.read_csv("build/info.csv", header=None)
data = data.sort_values([0,1])
print(data.head())
n_rows = data[0].nunique()
n_cols = data[1].nunique()
x = np.array(data[0]).reshape(n_rows,n_cols)
y = np.array(data[1]).reshape(n_rows,n_cols)
z = np.array(data[2]).reshape(n_rows,n_cols)
fig = plt.figure()
ax = plt.axes(projection='3d')
ax.set_xlabel("Length(bytes)")
ax.set_ylabel("Strides")
ax.set_zlabel("Bandwith(MB/s)")
ax.plot_surface(x, y, z, cmap=cm.coolwarm, linewidth=0, antialiased=False)

plt.show()
