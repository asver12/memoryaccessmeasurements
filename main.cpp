#include <vector>
#include <chrono>
#include <cmath>
#include <fstream>
#include <tuple>

#define input_data 1
#define to_byte 1024.0

using datatype = std::int64_t;

// size, stride, bandwidth ( MB/sec )
using Measurement = std::tuple<int, int, double>;

Measurement
messure_times(const datatype *memoryVector, datatype *destVector, const int num_accesses, const int stride_size,
              const int runs = 3, const int warmup_runs = 1) {

    // some warmup runs to get the cache working on a stable level:
    for (int j = 0; j < warmup_runs; ++j) {
        for (int i = 0; i < num_accesses; ++i) {
            destVector[i] = memoryVector[i * stride_size];
        }
    }

    // messure the time duration it needs to perform the accesses
    auto start = std::chrono::high_resolution_clock::now();

    for (int j = 0; j < runs; ++j) {
        for (int i = 0; i < num_accesses; ++i) {
            destVector[i] = memoryVector[i * stride_size];
        }
    }

    auto end = std::chrono::high_resolution_clock::now();

    // time duration
    const double sec = std::chrono::duration<double>(end - start).count();
    const int size = num_accesses * sizeof(datatype) / to_byte;

    // MB/s
    const double bandwidth = 1.024e-6 * size / sec;
    return Measurement(size, stride_size, bandwidth);
}

int main(int argc, char const *argv[]) {
    /*
     * Meassure the impact of striding on the bandwidth
     */

    // Max size in kbytes of array
    const int max_size = 20000;
    const int max_strides = 16;

    // allocate array with max size rounded up sizeof returns the size of a datatype in bytes
    std::vector<datatype> memoryVector(std::ceil((to_byte * max_size * max_strides) / sizeof(datatype)), input_data);
    std::vector<datatype> destVector(memoryVector.size());
    // allocate array for results
    std::vector<Measurement> result_vector;

    // set all sizes to calc
    std::vector<int> size_vector;
    for (int i = 2; i < max_size; i*=2)
        size_vector.push_back(i);

    std::vector<int> strides_vector;
    strides_vector.push_back(1);
    for (int i = 2; i <= max_strides; i*=2)
        strides_vector.push_back(i);

    // pragma omp parallel for only works with "normal for loops"
#pragma omp parallel for schedule(dynamic)
    for (int i = 0; i < size_vector.size(); ++i) {
        // Calculate the number of access depending on the given array size and size of the used datatype
        const int num_accesses = to_byte * size_vector[i] / sizeof(datatype);

        // Caculate striding depending on max strides
        for (int j = 0; j < strides_vector.size(); ++j) {
            auto result = messure_times(memoryVector.data(), destVector.data(), num_accesses, strides_vector[j]);
#pragma omp critical
            result_vector.push_back(std::move(result));
        }
    }

    std::ofstream ofs("info.csv");
    for (const auto &[stride, size, duration] : result_vector)
        ofs << stride << ',' << size << ',' << duration << '\n';

    return 0;

}
